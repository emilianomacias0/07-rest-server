const jwt = require('jsonwebtoken');

//=============================
//  Verificar token
//=============================

let verificaToken = (req, res, next) => {
    let token = req.get('token');
    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err
            })
        }

        req.usuario = decoded.usuario
        next();
    })
}

//=============================
//  Verificar admin role
//=============================
let verificaRole = (req, res, next) => {
    let usuario = req.usuario;
    if ('ADMIN_ROLE' !== usuario.role) {
        return res.json({
            ok: false,
            err: {
                message: 'El usuario no es Administrador'
            }
        })
    }
    next()
}

//==================================
// Verifica token url
//==================================
let verificaTokenImg = (req, res, next) => {
    let token = req.query.token;

    jwt.verify(token, process.env.SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err: {
                    message: 'Token no valido'
                }
            })
        }

        req.usuario = decoded.usuario
        next();
    })

}

module.exports = {
    verificaToken,
    verificaRole,
    verificaTokenImg
}