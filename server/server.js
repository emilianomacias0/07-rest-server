const express = require('express')
    // Using Node.js `require()`
const mongoose = require('mongoose');

const bodyParser = require('body-parser')

const path = require('path');

require('./config/config')


const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//habilitar public
app.use(express.static(path.resolve(__dirname, '../public')));
console.log(path.resolve(__dirname, '../public'));
//mongoose.connect('mongodb://192.168.1.2:27017/cafe', {
mongoose.connect(process.env.URLDB, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
}, (err, res) => {
    if (err) throw err;

    console.log('Base de datos conectada');
});
app.use(require('./routes/index'))

app.listen(process.env.PORT, () => {
    console.log('escuchando puerto: ', process.env.PORT)
})