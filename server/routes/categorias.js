const express = require('express');

let { verificaToken, verificaRole } = require('../middlewares/auth')

let app = express()

let Categoria = require('../models/categoria')

//====================
//Devuelve las categorias
//====================
app.get('/categoria', verificaToken, (req, res) => {
    Categoria.find({})
        .sort('nombre')
        .populate('usuario', 'nombre email')
        .exec((err, categorias) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            Categoria.countDocuments({}, (err, conteo) => {
                res.json({
                    ok: true,
                    categorias,
                    conteo
                });
            });
        })
})

//====================
//Mostar una categoria por id
//====================

app.get('/categoria/:id', verificaToken, (req, res) => {
    let id = req.params.id
    Categoria.findOne({ _id: id }).exec((err, categoria) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            categoria: categoria
        })
    })
})

//====================
// Crea una nueva categoria
//====================
app.post('/categoria', verificaToken, (req, res) => {
        let body = req.body;
        console.log(req);
        let idUsuario = req.usuario._id
        let categoria = new Categoria({
            usuario: idUsuario,
            nombre: body.nombre
        })
        categoria.save((err, categoriaDb) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                })
            }
            //usuarioDB.usuario = null
            res.json({
                ok: true,
                categoria: categoriaDb
            })

        })
    })
    //====================
    //Actualiza la categoria
    //====================

app.put('/categoria/:id', (req, res) => {
    let id = req.params.id
    let body = req.body
    let newCategoria = {
        nombre: body.nombre
    }
    Categoria.findByIdAndUpdate(id, newCategoria, { new: true, runValidators: true }, (err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }

        res.json({
            ok: true,
            categoria: categoriaDB
        })
    })
})

//====================
//Elimina categoria
//====================

app.delete('/categoria/:id', [verificaToken, verificaRole], (req, res) => {
    let id = req.params.id
    Categoria.findByIdAndRemove(id, (err, categoriaDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if (!categoriaDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'NO exixte la categoria en BD'
                }
            })
        }
        res.json({
            ok: true,
            categoria: categoriaDB
        })
    })
})
module.exports = app;