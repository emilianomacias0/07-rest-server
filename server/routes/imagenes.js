const express = require('express');
const path = require('path');
const fs = require('fs');
const { verificaTokenImg } = require('../middlewares/auth')

let app = express();

app.get('/imagen/:tipo/:img', verificaTokenImg, (req, res) => {
    let tipo = req.params.tipo;
    let imagen = req.params.img;

    let pathImg = path.resolve(__dirname, `../../uploads/${tipo}/${imagen}`);
    let noImagePath = path.resolve(__dirname, '../assets/no-image.jpeg');
    if (fs.existsSync(pathImg)) {
        res.sendFile(pathImg);
    } else {
        res.sendFile(noImagePath);
    }

});

module.exports = app;