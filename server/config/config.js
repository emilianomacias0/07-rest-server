//Puerto
//const port = process.env.PORT || 3000;

process.env.PORT = process.env.PORT || 3000;


//Entorno

process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//Expiracion token 
//60 segundos
//60 minutos
// 24 horas
// 30 dias

process.env.CADUCIDAD_TOKEN = '48h';

//SEED

process.env.SEED = process.env.SEED || 'este-es-el-seed-desarrollo';

// Base de datos

let urlDB;

if (process.env.NODE_ENV === 'dev') {
    //urlDB = 'mongodb://192.168.1.2:27017/cafe'
    urlDB = 'mongodb://caferoot:cafer00t@ds261077.mlab.com:61077/cafe'
} else {
    // urlDB = process.env.MONGO_URL;
    urlDB = 'mongodb://caferoot:cafer00t@ds261077.mlab.com:61077/cafe'
}

process.env.URLDB = urlDB;

//==========================
//  Google client ID
//==========================

process.env.GOOGLEID = process.env.GOOGLEID || '511736185008-spklqu75c2rbbpkoa592md44g8fuckr6.apps.googleusercontent.com';