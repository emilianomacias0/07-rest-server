const mongoose = require('mongoose');


let Schema = mongoose.Schema;

let categoriaSchema = new Schema({
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: [true, 'El id usuario es necesario'],
    },
    nombre: {
        type: String,
        unique: true,
        required: [true, 'La categoria es necesaria']
    },
    estado: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Categoria', categoriaSchema);